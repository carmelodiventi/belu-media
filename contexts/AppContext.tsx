import { createContext, useContext, useReducer } from "react";
import { appReducer, initialState } from 'reducers';

export const AppContext = createContext(null);

export const AppProvider = ({ children }) => {
    const value = useAppProvider();
    return <AppContext.Provider value={value}> {children}</AppContext.Provider>
}

export const useAppContext = () => {
    return useContext(AppContext);
}

export const createAction = (type: any, payload: any) => {
    return {
        type,
        payload
    };
};


const useAppProvider = () => {

    const [{ navigation, cursor, team }, dispatch] = useReducer(appReducer, initialState);

    return {
        navigation,
        cursor,
        team,
        dispatch
    }
}