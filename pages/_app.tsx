import { AppProps } from 'next/app'
import Layout from '@components/layout';
import { AppProvider } from '../contexts/AppContext';
import "../styles.scss";

function Aurora({ Component, pageProps, router }: AppProps) {
    return (
        <AppProvider>
            <Layout>
                <Component {...pageProps} key={router.route} />
            </Layout>
        </AppProvider>
    )
}

export default Aurora;