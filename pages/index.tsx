import { NextPageContext } from 'next';

import Head from '../components/head';
import PageContent from '../components/pageContent';
import request from 'graphql-request';
import { queryPage } from 'graphql/queries';
import Section from '@components/section';
import Block from '@components/block';


interface IProps {
  data: {
    page: {
      title: string;
      description: string;
      template: string;
      sections: [{
        id: string;
        type: string;
        isScrollable: boolean;
      }]
    }
  }
}

const Home = ({ data: { page: { title, description, template, sections } } }: IProps) => {

  return (
    <>
      <Head title={title} description={description} />
      <PageContent template={template} >
        {sections.map((section) => {
          return (
            <Section {...section} isScrollable={section.isScrollable} type={section.type} key={section.id}>
              <Block {...section} type={section.type} />
            </Section>
          )
        })}
      </PageContent>
    </>
  )
}

export async function getServerSideProps(ctx: NextPageContext) {

  const data = await request(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, queryPage, { slug: "" });
  return { props: { data } }

}

export default Home;