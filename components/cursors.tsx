import { useEffect, useRef } from 'react';
import { useMousePosition } from '../hooks/useMousePosition';
import { gsap } from 'gsap';
import { useAppContext } from 'contexts/AppContext';

const Cursors = () => {
    const outerCursorRef = useRef<HTMLDivElement | null>(null);
    const position = useMousePosition();
    const { cursor: { style } } = useAppContext();

    useEffect(() => {
        gsap.set(outerCursorRef.current, {
            x: position.x,
            y: position.y
        })
    }, [position.x, position.y])

    return (
        <div className={`circle-cursor ${style}`} ref={outerCursorRef}>
            <div className="circle-cursor--inner circle-cursor--outer"></div>
            <div className="circle-cursor--inner cursor__main"></div>
            <div className="circle-cursor--inner cursor__video">
                <img src="./img/music-player-play.svg" />
            </div>
            <div className="circle-cursor--inner cursor__gallery">
                <img src="./img/chevron-left-white.svg" />
                <img src="./img/chevron-right-white.svg" />
            </div>
            <div className="circle-cursor--inner cursor__close">
                <img src="./img/close-white.svg" />
            </div>
            <div className="circle-cursor--inner cursor__details">
                <img src="./img/details.svg" />
            </div>
        </div>
    )

}


export default Cursors;