import { useState, useEffect, useRef } from 'react';
import { useInView } from 'react-intersection-observer';
import { gsap, TweenLite } from 'gsap';
import { useAppContext } from 'contexts/AppContext';


interface IProps {
    media: {
        mime: string;
        url: string;
        id: string;
    };
    content: string;
}

const Video = ({ media, content }: IProps) => {

    const [ref, inView] = useInView({ threshold: .5 });
    const { dispatch } = useAppContext();
    const blockVideoRef = useRef<HTMLDivElement | null>(null);
    const videoRef = useRef<HTMLVideoElement | null>(null);
    const textOverlayRef = useRef<HTMLDivElement | null>(null);
    const [tweenBlockVideo] = useState(gsap.timeline({ paused: true }));
    const [tweenVideo] = useState(gsap.timeline({ paused: true }));

    let _readyToPlay = false;

    useEffect(() => {

        // videoRef.current.play();

        //https://greensock.com/forums/topic/16689-background-video-sync-with-timeline-in-banner/


        videoRef.current.addEventListener('play', _onPlay);
        videoRef.current.addEventListener('waiting', _onWaiting);
        videoRef.current.addEventListener('pause', _onWaiting);
        videoRef.current.addEventListener('ended', _onEnded);
        videoRef.current.addEventListener('timeupdate', _onUpdate);
        videoRef.current.addEventListener('mouseenter', _onMouseOver);
        videoRef.current.addEventListener('mouseleave', _onMouseOut);


        tweenBlockVideo
            .fromTo(blockVideoRef.current,
                { scale: 1.5, autoAlpha: 0, duration: .5 },
                { scale: 1, autoAlpha: 1, duration: .5 }
            )
            .play();


        tweenVideo
            .to(textOverlayRef.current.children[0], // getting 
                { opacity: 0 }, Math.round(videoRef.current.duration / 8.5)
            )

            .to(textOverlayRef.current.children[1], // keeping
                { opacity: 1 }, Math.round(videoRef.current.duration / 8)
            )
            .to(textOverlayRef.current.children[1], // keeping
                { opacity: 0 }, Math.round(videoRef.current.duration / 5)
            )

            .to(textOverlayRef.current.children[2], // Is the hard
                { opacity: 1 }, Math.round(videoRef.current.duration / 5.2)
            )
            .to(textOverlayRef.current.children[2], // Is the hard
                { opacity: 0 }, Math.round(videoRef.current.duration / 4.2)
            )

            .to(textOverlayRef.current.children[3], // what we say
                { opacity: 1 }, Math.round(videoRef.current.duration / 4)
            )
            .to(textOverlayRef.current.children[3], // what we say
                { opacity: 0 }, Math.round(videoRef.current.duration / 3)
            )

            .to(textOverlayRef.current.children[4], // or how we say
                { opacity: 1 }, Math.round(videoRef.current.duration / 3.2)
            )
            .to(textOverlayRef.current.children[4], // or how we say
                { opacity: 0 }, Math.round(videoRef.current.duration / 2.5)
            )

            .to(textOverlayRef.current.children[5], // but about how
                { opacity: 1 }, Math.round(videoRef.current.duration / 2.5)
            )
            .to(textOverlayRef.current.children[5], // but about how
                { opacity: 0 }, Math.round(videoRef.current.duration / 2.2)
            )

            .to(textOverlayRef.current.children[6], // we make them feel
                { opacity: 1 }, Math.round(videoRef.current.duration / 2.2)
            )
            .to(textOverlayRef.current.children[6], // we make them feel
                { opacity: 0 }, Math.round(videoRef.current.duration / 1.9)
            )

            .to(textOverlayRef.current.children[7], // and the emotion
                { opacity: 1 }, Math.round(videoRef.current.duration / 1.9)
            )
            .to(textOverlayRef.current.children[7], // and the emotion
                { opacity: 0 }, Math.round(videoRef.current.duration / 1.6)
            )

            .to(textOverlayRef.current.children[8], // that we create
                { opacity: 1 }, Math.round(videoRef.current.duration / 1.8)
            )
            .to(textOverlayRef.current.children[8], // that we create
                { opacity: 0 }, Math.round(videoRef.current.duration / 1.6)
            )
            .to(textOverlayRef.current.children[9], // getting
                { opacity: 1 }, Math.round(videoRef.current.duration / 1.5)
            )
            .to(textOverlayRef.current.children[9], // getting
                { opacity: 0 }, Math.round(videoRef.current.duration / 1.3)
            )

            .to(textOverlayRef.current.children[10], // making people listen
                { opacity: 1 }, Math.round(videoRef.current.duration / 1.25)
            )
            .to(textOverlayRef.current.children[10], // making people listen
                { opacity: 0 }, Math.round(videoRef.current.duration / 1.15)
            )

            .to(textOverlayRef.current.children[11], // is what we do
                { opacity: 1 }, Math.round(videoRef.current.duration / 1.1)
            )
            .to(textOverlayRef.current.children[11], // is what we do
                { opacity: 0 }, Math.round(videoRef.current.duration / 1)
            )



        return () => {

            videoRef.current.removeEventListener('play', _onPlay);
            videoRef.current.removeEventListener('waiting', _onWaiting);
            videoRef.current.removeEventListener('pause', _onWaiting);
            videoRef.current.removeEventListener('ended', _onEnded);
            videoRef.current.removeEventListener('timeupdate', _onUpdate);

        }

    }, [])

    useEffect(() => {

        tweenBlockVideo.reversed(!inView);
        if (!inView) videoRef.current.pause();

    }, [inView])


    //https://webkit.org/blog/6784/new-video-policies-for-ios/


    const _onPlaying = () => {
        if (_readyToPlay) {
            tweenVideo.play()
        } else {
            // Firefox does not seem to fire the 'loadedmetadata' event so, we wait for the first call to playing dispatched from the video element
            tweenVideo.duration(videoRef.current.duration);
            tweenVideo.play()
            _readyToPlay = true;
        }
    }

    const playVideo = () => {
        videoRef.current.play()
    }

    const _onPlay = () => {
        tweenVideo.play()
    }

    const _onWaiting = () => {
        tweenVideo.pause();
    }

    const _onEnded = () => {
        tweenVideo.pause(0);
    }

    const _onUpdate = () => {
        tweenVideo.time(videoRef.current.currentTime);
    }

    const _onMouseOver = () => {
        dispatch({ type: "CHANGE_CURSOR", payload: { style: 'cursor--hover-video' } })
    }

    const _onMouseOut = () => {
        dispatch({ type: "DEFAULT_CURSOR" })
    }


    return (
        <div className="section__inner" ref={ref}>

            <div className="block--text" ref={textOverlayRef}>
                <div className="text--overlay">Getting  <span className="text-yellow">attention</span> is <span className="text-highlighted">easy.</span></div>
                <div className="text--overlay">Keeping someone <span className="text-yellow">listings</span> </div>
                <div className="text--overlay">Is the hard <span className="text-yellow">part</span> </div>
                <div className="text--overlay">It's <span className="text-yellow">not</span> what we <span className="text-highlighted">say</span> </div>
                <div className="text--overlay">Or <span className="text-yellow">how</span> we <span className="text-highlighted">say</span> it</div>
                <div className="text--overlay">But <span className="text-yellow">about</span> <span className="text-highlighted">how,</span></div>
                <div className="text--overlay">We make them <span className="text-yellow">feel.</span></div>
                <div className="text--overlay">And the <span className="text-yellow text-right">emotion</span></div>
                <div className="text--overlay">That we <span className="text-right">create</span></div>
                <div className="text--overlay">Getting  <span className="text-yellow">attention</span> is the<span className="text-highlighted">easy</span> part.</div>
                <div className="text--overlay">Making people <span className="text-yellow">listen</span></div>
                <div className="text--overlay">is <span className="text-yellow">what</span> we do.</div>
            </div>

            <div
                className="block--video"
                ref={blockVideoRef}>

                <video
                    playsInline controls={false}
                    className="video-media"
                    preload="metadata"
                    onClick={playVideo}
                    ref={videoRef}
                >
                    <source src={"/videos/output.mp4"} type="video/mp4"></source>
                </video>

            </div>
        </div>
    )

}

export default Video;