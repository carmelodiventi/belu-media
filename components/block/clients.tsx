import { useState, useEffect, useRef } from 'react';
import { useInView } from 'react-intersection-observer';
import gsap from 'gsap';

const Clients = ({ subtitle, items }) => {

    const [ref, inView] = useInView({ threshold: .5 });
    const [tweenClients] = useState(gsap.timeline({ paused: true }));
    const logosRef = [];
    const subtitleRef = useRef<HTMLHeadingElement | null>(null)

    
    useEffect(() => {

        tweenClients
        .to(subtitleRef.current,{
            y: 0,
            autoAlpha: 1,
            duration: 1
        })
        .to(logosRef, {
            delay: 1.2,
            duration: 1,
            opacity: .8,
            y: 0,
            stagger: 0.15
        }).reverse()

    }, [])

    useEffect(() => {
        tweenClients.reversed(!inView)
    }, [inView])

    return (
        <div className="section__inner" ref={ref}>
            <div className="block--logos">
                <h2 className="block--logos__title" ref={subtitleRef}>{subtitle}</h2>
                {items.map(({ id, name, logo }, idx : number) => <div className="logo" key={id} ref={ el => logosRef[idx] = el}>{logo && <img src={logo.formats.thumbnail.url} alt={name} />}</div>)}
            </div>
        </div>
    )

}

export default Clients;