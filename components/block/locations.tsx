import Location from './locations/location';

const Locations = ({ locations }) => {


    return (
        <div className="section__inner">
            <div className="contact-us__list">
                <div className="contact-us__list__content">
                    {locations.map(({ id, name, phone, email, mapUrl, address, image }) => <Location name={name} phone={phone} email={email} mapUrl={mapUrl} address={address} image={image} key={id} />)}
                </div>
            </div>
        </div>
    );
};

export default Locations;