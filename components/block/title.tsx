import React, { useEffect, useRef, useState } from 'react';
import { useInView } from 'react-intersection-observer';
import gsap from 'gsap';
import { split } from '../../lib/splitText';

interface IProps {
    title: string;
    content: string;
}

const Title = ({ title, content }: IProps) => {

    const highlightRef = useRef(null);
    const headerTextRef = useRef(null);
    const [tweenTitle] = useState(gsap.timeline({ paused: true }))
    const [ref, inView] = useInView();

    useEffect(() => {

        split({ element: headerTextRef.current })

        tweenTitle
        .to(highlightRef.current,{
            duration: .5, 
            opacity: 1
        })
        .to([headerTextRef.current.children], {
            duration: .5, 
            y: 0,
            opacity: 1,
            stagger: 0.05
        })
        .reverse()

    }, [])

    useEffect(() => {
        tweenTitle.reversed(!inView)
    }, [inView])

    return (
        <div className="title" ref={ref}>
            <div className="title--wrapper">
                <h1 className="title--header__title">
                    <span className="title--header__highlight" ref={highlightRef}>
                        {title}
                    </span>
                    <span className="title--header__text" ref={headerTextRef}>
                        {content}
                    </span>
                </h1>

            </div>
        </div>
    );
};

export default Title;