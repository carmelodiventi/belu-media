import React, { Component, createRef, MouseEvent } from 'react';
import { gsap } from 'gsap'
import { clamp } from 'lodash'
import TeamExplore from '@components/block/team/teamExplore';
import TeamItem from '@components/block/team/teamItem';
import TeamScrollIndicator from '@components/block/team/teamScrollIndicator';
import { InView } from 'react-intersection-observer';
import { AppContext } from 'contexts/AppContext';

interface IProps {
    items: [{
        id: string;
        name: string;
        photo: any;
    }]
}

interface IState {
    progress: number
}

class TeamCarousel extends Component<IProps, IState>{

    static contextType = AppContext;

    items: any[];
    element: React.RefObject<HTMLDivElement>;
    itemsList: React.RefObject<HTMLDivElement>;
    indicatorCircle: React.RefObject<HTMLOrSVGElement>;
    index: number;
    isActive: boolean;
    isHolding: boolean;
    x: { current: number; last: number; };
    itemsLength: number;
    position: { current: number; target: number; value: number; };
    clamp: { minimum: number; maximum: number; };
    width: number;
    animation: number;
    timeout: NodeJS.Timeout;
    timeline: gsap.core.Timeline;
    dispatch: any;

    constructor(props: IProps) {

        super(props);

        this.element = createRef();
        this.itemsList = createRef();
        this.indicatorCircle = createRef();
        this.timeline = gsap.timeline({ paused: true })
        this.items = [];
        this.index = 3;
        this.isActive = false;
        this.isHolding = false;
        this.x = {
            current: 0,
            last: 0
        }
        this.width = 0;
        this.timeout = null;

        this.state = {
            progress: 0
        }

        this.onUpdate = this.onUpdate.bind(this);
        this.enable = this.enable.bind(this);
        this.disable = this.disable.bind(this);

        this.onDown = this.onDown.bind(this);
        this.onMove = this.onMove.bind(this);
        this.onUp = this.onUp.bind(this);

        this.onItemEnter = this.onItemEnter.bind(this);
        this.onItemLeave = this.onItemLeave.bind(this);
    }


    componentDidMount() {
        const element = this.element.current;
        const parent = element.parentNode;

        this.itemsLength = this.items.length - 1;

        this.timeline
            .to(this.items, {
                duration: .8,
                y: 0,
                autoAlpha: 1,
                ease: "power3.easeIn",
                stagger: 0.15
            })
            .reverse()

        this.items.forEach((item, idx) => {
            item.index = idx;
            this.width = this.width + item.clientWidth;
            item.addEventListener('mouseover', this.onItemEnter, true)
            item.addEventListener('mouseout', this.onItemLeave, true)
        })

        this.position = {
            current: 0,
            target: this.items[this.index].offsetLeft + this.items[this.index].clientWidth / 2,
            value: this.items[this.index].offsetLeft + this.items[this.index].clientWidth / 2
        }

        this.clamp = {
            minimum: 0, //this.items[0].clientWidth / 2,
            maximum: this.width //this.width - this.items[this.itemsLength].clientWidth / 2
        }

        this.setState({
            progress: Math.round(this.position.target / this.width * 100)
        })

        this.enable();

        parent.addEventListener('mousedown', this.onDown, true)
        parent.addEventListener('mousemove', this.onMove, true)
        parent.addEventListener('mouseup', this.onUp, true)

        element.addEventListener('touchstart', this.onDown)
        element.addEventListener('touchmove', this.onMove)
        element.addEventListener('touchend', this.onUp)
    }

    componentWillUnmount() {
        const element = this.element.current;
        const parent = element.parentNode;

        this.disable();
        this.timeline.kill();

        parent.removeEventListener('mousedown', this.onDown)
        parent.removeEventListener('mousemove', this.onMove)
        parent.removeEventListener('mouseup', this.onUp)

        element.removeEventListener('touchstart', this.onDown)
        element.removeEventListener('touchmove', this.onMove)
        element.removeEventListener('touchend', this.onUp)

        this.items.forEach((item, idx) => {
            item.index = idx;
            item.removeEventListener('mouseover', this.onItemEnter)
            item.removeEventListener('mouseout', this.onItemLeave)
        })
    }

    showItems(inView: boolean, entry: any) {
        if (entry.intersectionRatio > 1 && inView) {
            this.timeline.play()
        }
        else {
            this.timeline.reversed(!inView)
        }
    }

    onDown(event: any) {

        event.preventDefault();

        if (window.innerWidth < 768) {
            event.stopPropagation()
        }

        if (!this.isActive) return;

        this.context.dispatch({ type: 'CHANGE_CURSOR', payload: { style: "cursor--hover-link" } })

        this.position.current = this.position.target;
        this.x.current = event.touches ? event.touches[0].clientX : event.clientX;
        this.isHolding = true;

        clearTimeout(this.timeout)
        this.itemsList.current.classList.add('team--list--dragging')
        this.element.current.classList.add('team--content--dragging')
    }

    onMove(event: any) {
        event.preventDefault()

        if (window.innerWidth < 768) {
            event.stopPropagation()
        }

        if (!this.isHolding || !this.isActive) return

        this.x.last = event.touches ? event.touches[0].clientX : event.clientX;
        this.position.target = this.position.current + ((this.x.current - this.x.last) * 1.25);
        this.position.target = clamp(this.position.target, this.clamp.minimum, this.clamp.maximum);

        this.setState({
            progress: Math.round(this.position.target / this.width * 100)
        })

    }

    onUp(event: any) {

        event.preventDefault()
        event.stopPropagation()

        if (!this.isActive) return

        this.onCheck()

        this.isHolding = false;
        this.context.dispatch({ type: 'DEFAULT_CURSOR' })

        this.timeout = setTimeout(() => {
            this.itemsList.current.classList.remove('team--list--dragging')
            this.element.current.classList.remove('team--content--dragging')
        }, 500)
    }

    onCheck() {
        const index = Math.floor(this.position.target / this.width * this.items.length)
        const width = this.width / this.items.length

        this.position.target = width * index + width / 2
    }

    onUpdate() {
        if (!this.isActive) return

        this.position.value += (this.position.target - this.position.value) * 0.05
        this.position.value = clamp(this.position.value, this.clamp.minimum, this.clamp.maximum)

        this.width = 0

        this.items.forEach((item) => this.width += item.clientWidth)

        this.index = Math.floor(this.position.value / this.width * this.items.length)

        this.items.forEach((item, index) => {
            if (index === this.index) {
                item.classList.add('team-item__active')
            } else {
                item.classList.remove('team-item__active')
            }
        })

        this.clamp = {
            minimum: 0, //this.items[0].clientWidth / 2,
            maximum: this.width //this.width - this.items[this.itemsLength].clientWidth / 2
        }

        gsap.set(this.itemsList.current, {
            x: -this.position.value
        })

        this.animation = window.requestAnimationFrame(this.onUpdate)
    }

    enable() {
        this.isActive = true
        this.onUpdate()
    }

    disable() {
        this.isActive = false
        this.onUpdate()
    }

    onItemEnter(event) {

        const { currentTarget } = event;

        currentTarget.classList.add('team-item__huge');

        if (currentTarget.previousElementSibling && currentTarget.previousElementSibling.tagName === 'DIV') {
            currentTarget.previousElementSibling.classList.add('team-item__large')
        }

        if (currentTarget.nextElementSibling && currentTarget.nextElementSibling.tagName === 'DIV') {
            currentTarget.nextElementSibling.classList.add('team-item__large')
        }

        currentTarget.style.zIndex = 2;

        this.context.dispatch({
            type: "SHOW_MEMBER_INFO",
            payload: {
                index: currentTarget.index
            }
        })

    }

    onItemLeave = (event) => {

        const { currentTarget } = event;

        currentTarget.classList.remove('team-item__huge');

        if (currentTarget.previousElementSibling && currentTarget.previousElementSibling.tagName === 'DIV') {
            currentTarget.previousElementSibling.classList.remove('team-item__large')
        }

        if (currentTarget.nextElementSibling && currentTarget.nextElementSibling.tagName === 'DIV') {
            currentTarget.nextElementSibling.classList.remove('team-item__large')
        }

        currentTarget.style.zIndex = '';

        this.context.dispatch({
            type: "HIDE_MEMBER_INFO",
            payload: {
                index: currentTarget.index
            }
        })
    }
    

    render() {

        const { items } = this.props;
        const { progress } = this.state;

        return (
            <InView as="div" onChange={(inView, entry) => this.showItems(inView, entry)}>

                <div className="team--content" ref={this.element}>
                    <div className="team--box">
                        <div className="team--list" ref={this.itemsList}>
                            {items.map(({ id, photo, name }, idx: number) => <TeamItem photo={photo} name={name} index={idx} key={id} ref={el => this.items[idx] = el} />)}
                        </div>
                    </div>
                    <TeamScrollIndicator stroke={1} radius={150} progress={progress} />
                </div>

                <TeamExplore items={items} />

            </InView>
        );
    }
}

export default TeamCarousel;