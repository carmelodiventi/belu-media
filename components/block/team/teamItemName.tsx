import { useEffect, useRef } from 'react';
import { split } from '@lib/splitText';

interface IProps {
    name: string;
}

const TeamItemName = ({ name } : IProps) => {

    let teamName = useRef(null)

    useEffect(() => {
        split({ element: teamName.current })
    }, [])

    return (
        <div className="team__name" ref={teamName}>
            {name}
        </div>
    );
};

export default TeamItemName;