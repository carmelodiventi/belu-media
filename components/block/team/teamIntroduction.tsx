import { Converter } from "showdown";

interface IProps {
    title: string;
    content?: string;
}

const TeamIntroduction = ({ title, content } : IProps) => {

    const converter = new Converter();

    return (
        <div className="team--indroduction">
            <div className="team--indroduction__inner">
                <div className="team--indroduction__title">
                    <h1>
                        {title}
                    </h1>
                </div>
                {content &&
                    <div className="team--indroduction__content">
                        {converter.makeHtml(content)}
                    </div>
                }
            </div>
        </div>
    );
};

export default TeamIntroduction;