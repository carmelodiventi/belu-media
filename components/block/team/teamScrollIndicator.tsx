import React from 'react';

const TeamScrollIndicator = ({ progress, radius, stroke }) => {

    const normalizedRadius = radius - stroke * 2;
    const circumference = normalizedRadius * 2 * Math.PI;
    const strokeDashoffset = circumference - progress / 100 * circumference;
  

    return (
        <div className="team__indicator">
            <svg height={radius * 2}  width={radius * 2} fill="transparent">
                <circle cx={radius} cy={radius} r={normalizedRadius} stroke="#6c756f" strokeWidth={stroke} />
                <circle cx={radius} cy={radius} r={normalizedRadius} fill="transparent" stroke="white" strokeWidth={stroke} strokeDasharray={circumference + ' ' + circumference} style={{ strokeDashoffset }} />
            </svg>
        </div>
    );
};

export default TeamScrollIndicator;