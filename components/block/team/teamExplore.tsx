import { useState, useEffect, useRef } from 'react';
import { gsap } from 'gsap';
import TeamInformationsItem from './teamInformationsItem';
import TeamExploreIndicator from './teamExploreIndicator';
import { useAppContext } from 'contexts/AppContext';

interface ChildNode extends Node {
    animation: any;
}

const TeamExplore = ({ items }) => {

    const [exploreTween] = useState(gsap.timeline({ paused: true }));
    const exploreIndicatorRef = useRef(null);
    const exploreInformationsRef = useRef<any | null>(null);
    const { team: { isHover, active } } = useAppContext();

    useEffect(() => {

        const indicatorCircleLeft = exploreIndicatorRef.current.children[0];
        const indicatorLine = exploreIndicatorRef.current.children[1];
        const indicatorCircleRight = exploreIndicatorRef.current.children[2];
        const indicatorLabel = exploreIndicatorRef.current.children[3];
        const exploreInformationsItems = exploreInformationsRef.current.childNodes;

        exploreTween
            .to(indicatorLine, {
                duration: 0.5,
                width: 0,
                ease: 'power3.easeOut'
            })
            .to([indicatorCircleLeft, indicatorCircleRight], {
                duration: 0.5,
                autoAlpha: 0,
                ease: 'power3.easeOut',
                stagger: {
                    amount: 0.15
                }
            })
            .to(indicatorLabel, {
                duration: 0.5,
                y: 20,
                autoAlpha: 0,
                ease: 'power3.easeOut'
            }, .2)
            .reverse();

        exploreInformationsItems.forEach((info : any) => {
            // create a new timeline for each element that will be fired once redux will dispatch that the element isHovered
            const infoTween = gsap.timeline({ paused: true });
            const header_container = info.children[0];
            const description_container = info.children[1];

            infoTween
                .set(info, { delay: 1, autoAlpha: 1 })
                .to([header_container, description_container], {
                    duration: .8,
                    opacity: 1,
                    y: 0,
                    ease: "power3.out",
                    stagger: {
                        amount: 0.15
                    }
                })

            info.animation = infoTween;

        });


        return () => {
            exploreTween.kill()
        }

    }, [])

    useEffect(() => {

        exploreTween.reversed(!isHover)

        if (active === null) return;

        if (isHover) {
            exploreInformationsRef.current.childNodes[active].animation.play()
        }
        else {
            exploreInformationsRef.current.childNodes[active].animation.reverse()
        }


    }, [isHover])

    return (
        <div className="team__explore--container">
            <div className="team__feedback"></div>
            <div className="team__explore">
                <TeamExploreIndicator ref={exploreIndicatorRef} />
            </div>
            <div className="team__informations" ref={exploreInformationsRef}>
                {items.map(({ role, name, description }) => <TeamInformationsItem name={name} role={role} description={description}  key={name} />)}
            </div>
        </div>
    );
};

export default TeamExplore;

