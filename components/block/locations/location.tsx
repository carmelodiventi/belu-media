import React, { useEffect, useRef, useState } from 'react';
import { useInView } from 'react-intersection-observer';
import { Converter } from 'showdown';
import { gsap } from 'gsap';

type Props = {
    name: string;
    image: {
        url: string;
        name: string;
        formats: any
    };
    phone: string;
    email: string;
    address: string;
    mapUrl: string;
}

const Location = ({ name, image, phone, email, address, mapUrl }: Props) => {

    const refTitle = useRef(null);
    const refMedia = useRef(null);
    const refImage = useRef(null);
    const refContacts = useRef(null);
    const refDescription = useRef(null);
    const refLink = useRef(null);

    const [locationTween] = useState(gsap.timeline({ paused: true }));
    const [locationRef, inView, entry] = useInView({ threshold: .1 });
    const converter = new Converter();

    useEffect(() => {

        locationTween
            .to(refTitle.current, {
                autoAlpha: 1,
                duration: 1
            }, 0)
            .to(refMedia.current, {
                duration: 1,
                width: 0
            }, 0.8)
            .to(refImage.current, {
                duration: 1,
                scale: 1
            },0.8)
            .to([refContacts.current.children], {
                y: 0,
                duration: .7,
                autoAlpha: 1,
                stagger: .15
            })
            .to([refDescription.current, refLink.current], {
                y: 0,
                autoAlpha: 1,
                duration: .7,
                stagger: .15
            })
            .reverse()

    }, [])

    useEffect(() => {
        locationTween.reversed(!inView);
    }, [inView])

    return (
        <div className="contact-us__list__item" ref={locationRef}>
            <div className="contact-us__list__item__content">

                <div className="contact-us__list__name" ref={refTitle}>
                    <h2>{name}</h2>
                </div>

                <div className="contact-us__list__effect">

                    <div className="contact-us__list__media">
                        <div className="contact-us__list__cover" ref={refMedia}></div>
                        <img className="contact-us__list__image" src={image.url} alt={image.name} title={image.name} ref={refImage} />
                    </div>


                    <div className="contact-us__list__box">

                        <p className="contact-us__list__subtitle" ref={refContacts}>
                            <span>
                                <a href={`mailto:${email}`} rel="nofollow" target="_new">{email}</a>
                            </span>
                            <span>
                                <a href={`tel:${phone}`} rel="nofollow" target="_new">{phone}</a>
                            </span>
                        </p>

                        <p className="contact-us__list__description" dangerouslySetInnerHTML={{ __html: converter.makeHtml(address) }} ref={refDescription} />

                        <p className="contact-us__list__link" ref={refLink}>
                            <a href={`${mapUrl}`} rel="noopener" target="_blank" title="Google Maps"> View on Map </a>
                        </p>

                    </div>

                </div>
            </div>
        </div>
    );
};

export default Location;