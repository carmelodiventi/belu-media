import { useState, useEffect, useRef } from 'react';
import { useInView } from 'react-intersection-observer';
import { gsap } from 'gsap';
import { useAppContext } from 'contexts/AppContext';


interface IProps {
    media: {
        mime: string;
        url: string;
        id: string;
    };
    content: string;
}

const Video = ({ media, content }: IProps) => {

    const [ref, inView] = useInView({ threshold: .5 });
    const { dispatch } = useAppContext();
    const blockVideoRef = useRef<HTMLDivElement | null>(null);
    const videoRef = useRef<HTMLVideoElement | null>(null);
    const [tweenBlockVideo] = useState(gsap.timeline({ paused: true }));


    useEffect(() => {

        videoRef.current.addEventListener('mouseenter', _onMouseOver);
        videoRef.current.addEventListener('mouseleave', _onMouseOut);


        tweenBlockVideo
            .fromTo(blockVideoRef.current,
                { scale: 1.5, autoAlpha: 0, duration: .5 },
                { scale: 1, autoAlpha: 1, duration: .5 }
            )
            .play();

        return () => {

            videoRef.current.removeEventListener('mouseenter', _onMouseOver);
            videoRef.current.removeEventListener('mouseleave', _onMouseOut);

        }

    }, [])

    useEffect(() => {

        tweenBlockVideo.reversed(!inView);
        if (!inView) videoRef.current.pause();

    }, [inView])


    //https://webkit.org/blog/6784/new-video-policies-for-ios/


    const playVideo = () => {
        videoRef.current.currentTime = 0;
        videoRef.current.play()
    }

    const _onMouseOver = () => {
        dispatch({ type: "CHANGE_CURSOR", payload: { style: 'cursor--hover-video' } })
    }

    const _onMouseOut = () => {
        dispatch({ type: "DEFAULT_CURSOR" })
    }


    return (
        <div className="section__inner" ref={ref}>

            <div
                className="block--video"
                ref={blockVideoRef}>

                <video
                    controls={false}
                    className="video-media"
                    preload="metadata"
                    onClick={playVideo}
                    ref={videoRef}
                >
                    <source src={`${media.url}#t=8`} type={media.mime}></source>
                </video>

            </div>
        </div>
    )

}

export default Video;