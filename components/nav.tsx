import React, { useRef, useState, useEffect } from 'react'
import { withRouter, NextRouter } from 'next/router';
import { gsap } from 'gsap';
import { Converter } from 'showdown';
import Logo from './logo';
import ButtonCloseMenu from './menu/buttonCloseMenu';
import { UrlObject } from 'url';
import { useAppContext } from 'contexts/AppContext';


type NavProps = {
  router: NextRouter,
  pages: [
    {
      slug: string;
      title: string;
      name: string;
    }
  ],
  widgets: [
    {
      id: string;
      title: string;
      content: string;
    }
  ]

}

const Nav = ({ router, pages, widgets }: NavProps) => {

  let nav = useRef<HTMLDivElement | null>(null);
  let menuList = [];
  const [hoverNav, setHoverNav] = useState(false);
  const [menuTween] = useState(gsap.timeline({ paused: true }));
  const { navigation: { navOpen }, dispatch } = useAppContext();
  const converter = new Converter;

  // console.log(pages)

  useEffect(() => {

    if (nav.current) {
      const navigation__wrapper = nav.current.children[2];
      const navigation_inner = navigation__wrapper.children[0];
      const navigation_blinds = navigation__wrapper.children[1].children;
      const navigation_items = menuList;
      const navigation_details = navigation_inner.children[1].children;

      menuTween
        .to([nav.current, navigation_inner], {
          duration: 0,
          autoAlpha: 1
        })
        .to(navigation_blinds, {
          delay: .5,
          duration: 0.5,
          height: '100%',
          ease: "power3.easeOut",
          stagger: 0.07
        })
        .to([navigation_items, navigation_details], {
          duration: 0.3,
          y: 0,
          autoAlpha: 1,
          ease: "power3.easeOut",
          stagger: 0.05
        })
        .reverse()
    }

    router.events.on('routeChangeComplete', handleRouteChangeCompleted);

    return () => {
      router.events.off('routeChangeComplete', handleRouteChangeCompleted)
    }

  }, [])

  useEffect(() => {

    navOpen ? document.body.classList.add('nav-open') : document.body.classList.remove('nav-open');
    menuTween.reversed(!navOpen);

  }, [navOpen])

  useEffect(() => {
    hoverNav ? document.body.classList.add('hover-main-navigation') : document.body.classList.remove('hover-main-navigation');
  }, [hoverNav])

  const _onMouseEnter = () => {
    setHoverNav(true)
  }

  const _onMouseLeave = () => {
    setHoverNav(false)
  }

  const handleRouteChangeCompleted = () => {
    dispatch({ type: "TOGGLE_MENU", payload: { navOpen: false } })
  }

  const pushRoute = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>, pathname: string | UrlObject) => {
    e.preventDefault();
    router.push(pathname)
  }


  return (
    <div className="main-navigation" ref={nav}>
      <Logo />
      <ButtonCloseMenu />
      <div className="main-navigation__wrapper">
        <div className="main-navigation__inner">
          <div className="main-navigation__col main-navigation__col--left">
            <nav className="main-navigation__nav">
              <ul onMouseOver={_onMouseEnter} onMouseOut={_onMouseLeave}>
                {
                  pages.map(({ slug, title, name }: any, idx: number) =>
                    <li key={title} ref={el => (menuList[idx] = el)}>
                      <a href={slug || '/'} onClick={(e) => { pushRoute(e, slug || '/') }}>
                        <span>
                          {name}
                        </span>
                      </a>
                    </li>
                  )
                }
              </ul>
            </nav>
          </div>
          <div className="main-navigation__col main-navigation__col--right">
            {
              widgets.map(({ id, title, content }) => {
                return (
                  <div className="main-navigation__details" key={id}>
                    {title && <h4>{title}</h4>}
                    <div dangerouslySetInnerHTML={{ __html: converter.makeHtml(content) }} />
                  </div>
                )
              })
            }
          </div>
        </div>
        <div className="main-navigation__blinds">
          <div className="main-navigation__blind main-navigation__blind--left"></div>
          <div className="main-navigation__blind"></div>
          <div className="main-navigation__blind"></div>
          <div className="main-navigation__blind"></div>
          <div className="main-navigation__blind"></div>
          <div className="main-navigation__blind main-navigation__blind--right"></div>
        </div>
      </div>
    </div>
  )
}

export default withRouter(Nav);