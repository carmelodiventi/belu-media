import Text from './block/text';
import Video from './block/video';
import Clients from './block/clients';
import Team from './block/team';
import Locations from './block/locations';


const Block = (props: any) => {

    return (
        <>
            {
                {
                    ComponentBlocksBlockText:
                        <Text {...props} />,
                    ComponentBlocksBlockVideo:
                        <Video {...props} />,
                    ComponentBlocksBlockClients:
                        <Clients {...props} />,
                    ComponentBlocksBlockTeam:
                        <Team {...props} />,
                    ComponentBlocksBlockLocations:
                        <Locations {...props} />
                }
                [props.type]
            }
        </>
    );
};

export default Block;