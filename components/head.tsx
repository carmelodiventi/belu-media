import NextHead from 'next/head';

const Head = ({ title, description }) => (
    <NextHead>
        <meta charSet="UFT-8" />
        <title> {title || 'Aurora Demand'} </title>
        <meta name="description" content={description || ''} />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui" key="viewport" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={title || ''} />
        <meta property="og:image" content="img/share.png" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="theme-color" content="#232322" />
        <link rel="manifest" href="/manifest.json" />
        <link rel="apple-touch-icon" sizes="57x57" href="icons/apple-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="60x60" href="icons/apple-icon-60x60.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="icons/apple-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="icons/apple-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="icons/apple-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="icons/apple-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="icons/apple-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="icons/apple-icon-152x152.png" />
        <link rel="apple-touch-icon" sizes="180x180" href="icons/apple-icon-180x180.png" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta name="msapplication-TileImage" content="icons/ms-icon-144x144.png" />
        <meta name="theme-color" content="#ffffff"></meta>
    </NextHead>
)



export default Head;