import Wrapper from './wrapper';
import Preloader from './preloader';
import PageNav from '../containers/pageNav';
import Cursors from './cursors';
import withFontResize from '../lib/withFontResize';

type Props = {
    children: React.ReactChildren
}

const Layout = ({ children } : Props) => {

    return (
        <>
            <PageNav />
            <Wrapper>
                {children}
            </Wrapper>
            <Cursors />
            <Preloader />
        </>
    );

}

export default withFontResize(Layout);