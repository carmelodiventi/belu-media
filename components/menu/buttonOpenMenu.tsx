import React, { useState, useEffect, useRef } from 'react';
import { useAppContext } from 'contexts/AppContext';

const ButtonOpenMenu = () => {

    const [disabled, setDisabled] = useState(false);
    const { navigation: { navOpen }, dispatch } = useAppContext();
    const btnMenuRef = useRef<HTMLButtonElement | null>(null);

    useEffect(() => {

        btnMenuRef.current.addEventListener('mouseenter', _onMouseEnter, true)
        btnMenuRef.current.addEventListener('mouseleave', _onMouseLeave, true)

        return () => {
            btnMenuRef.current.removeEventListener('mouseenter', _onMouseEnter)
            btnMenuRef.current.removeEventListener('mouseleave', _onMouseLeave)
        }

    },[])

    const _onMouseEnter = () => {
        dispatch({ type: "CHANGE_CURSOR", payload: { style: 'cursor--hover-link' } })
    }

    const _onMouseLeave = () => {
        dispatch({ type: "DEFAULT_CURSOR" })
    }

    const _onClick = () => {
        disableMenu();
        dispatch({
            type: 'TOGGLE_MENU',
            payload: {
                navOpen: !navOpen
            }
        })
    }

    const disableMenu = () => {
        setDisabled(!disabled);
        setTimeout(() => {
            setDisabled(false)
        }, 1500)
    }

    return (
        <button
            className="menu-icon"
            type="button"
            disabled={disabled}
            onClick={_onClick}
            ref={btnMenuRef}
            >
            <svg width="20" height="10" viewBox="0 0 20 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                <line y1="5" x2="20" y2="5" strokeWidth="2" />
                <line y1="1" x2="20" y2="1" strokeWidth="2" />
                <line y1="9" x2="20" y2="9" strokeWidth="2" />
            </svg>
            Menu
        </button>
    );
};

export default ButtonOpenMenu;