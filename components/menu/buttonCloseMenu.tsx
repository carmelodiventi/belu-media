import React, { useState, useEffect, useRef } from 'react';
import { useAppContext } from 'contexts/AppContext';


const ButtonCloseMenu = () => {

    const btnMenuRef = useRef<HTMLButtonElement | null>(null);
    const [disabled, setDisabled] = useState(false);
    const { dispatch } = useAppContext();

    useEffect(() => {

        btnMenuRef.current.addEventListener('mouseenter', _onMouseEnter, true)
        btnMenuRef.current.addEventListener('mouseleave', _onMouseLeave, true)

        return () => {
            btnMenuRef.current.removeEventListener('mouseenter', _onMouseEnter)
            btnMenuRef.current.removeEventListener('mouseleave', _onMouseLeave)
        }

    }, [])


    const _onMouseEnter = () => {
        dispatch({ type: "CHANGE_CURSOR", payload: { style: 'cursor--hover-link' } })
    }

    const _onMouseLeave = () => {
        dispatch({ type: "DEFAULT_CURSOR" })
    }

    const _onClick = () => {
        disableMenu();
        dispatch({ type: "TOGGLE_MENU", payload: { navOpen: false } })
    }

    const disableMenu = () => {
        setDisabled(!disabled);
        setTimeout(() => {
            setDisabled(false)
        }, 1500)
    }

    return (
        <button
            className="menu-icon"
            type="button"
            disabled={disabled}
            onClick={_onClick}
            ref={btnMenuRef}>
            <svg width="16" height="16" viewBox="0 0 16 16">
                <line x1="1.35355" y1="0.646447" x2="15.4957" y2="14.7886" strokeWidth="2" />
                <line x1="0.646447" y1="14.7886" x2="14.7886" y2="0.646461" strokeWidth="2" />
            </svg>
            Close
        </button>
    );
};

export default ButtonCloseMenu;