import Logo from './logo';
import ButtonOpenMenu from './menu/buttonOpenMenu';
import Socials from './socials';
import ScrollDown from './scrollDown';

type Props = {
    isCurrentFooter: boolean;
}

const PageNavigation = ({ isCurrentFooter } : Props) => {

    return (
        <>
            <Logo />
            <ButtonOpenMenu />
            <Socials />
            <ScrollDown isCurrentFooter={isCurrentFooter} />
        </>
    );
};

export default PageNavigation;