const ScrollDown = ({ isCurrentFooter }) => (

    <div className={`page-scroller ${!isCurrentFooter ? 'page-scroller__visible' : 'page-scroller__hidden'}`}>
        {!isCurrentFooter &&
            <>
                <svg width="9" height="92" viewBox="0 0 9 92" fill="none">
                    <path d="M8.29688 0L8.29688 90.8914M8.4137 91L1 80.9365"/>
                </svg>
            </>
        }
    </div>

)

export default ScrollDown;