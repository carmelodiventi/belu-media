import React, { Component, createRef } from 'react';
import { Lethargy } from 'lethargy';
import { gsap } from 'gsap';
import { split } from '@lib/splitText';
import { randomInteger } from '../helpers/helper';
import { NextRouter, withRouter } from 'next/router';
import PageNavigation from './pageNavigation';
import PageFooter from '@containers/pageFooter';
import Title from '@components/title';

type Props = {
    router: NextRouter;
    template: string;
    children: React.ReactNodeArray | null;
}

type Lethargy = {
    check : ( evt : Event) => boolean
}

class PageContent extends Component<any, any> {

    pageContentRef = createRef<HTMLDivElement | null>();
    pageNavigationRef = createRef<HTMLDivElement | null>();
    pageFooterRef = createRef<HTMLDivElement | null>();
    sectionContainerRef = createRef<HTMLDivElement | null>();

    isAnimating: boolean;
    step: number;
    titles: any;
    y: {
        start: number;
        end: number;
    }
    lethargy: Lethargy;
    isTouchDown: boolean;

    constructor(props: Props) {
        super(props)

        this.state = {
            pageContent: null,
            pageNavigation: null,
            sectionContainer: null,
            pageFooter: null,
            sections: [],
            isCurrentFooter: false
        }

        this.isAnimating = false;
        this.step = 0;
        this.titles = [];
        this.y = {
            start: 0,
            end: 0
        }

        this.lethargy = new Lethargy();
        this.onMouseWheel = this.onMouseWheel.bind(this);
        this.onTouchStart = this.onTouchStart.bind(this)
        this.onTouchMove = this.onTouchMove.bind(this)
        this.onTouchEnd = this.onTouchEnd.bind(this)
    }

    componentDidMount() {

        const pageContent = this.pageContentRef.current;
        const pageNavigation = this.pageNavigationRef.current;
        const pageFooter = this.pageFooterRef.current;
        const sectionContainer = this.sectionContainerRef.current;
        const sections = this.getSections(this.props.children);

        const backgroundColor = sections[this.step].props.background;
        const color = sections[this.step].props.color;

        gsap.set(pageContent, { visibility: "visible", backgroundColor: backgroundColor, color: color })
        gsap.set(pageNavigation, { color: color });

        this.showTitle(sections[this.step])

        this.setState({
            pageContent,
            pageNavigation,
            pageFooter,
            sectionContainer,
            sections
        })

        document.addEventListener('mousewheel', this.onMouseWheel, { passive: true });
        document.addEventListener('wheel', this.onMouseWheel, { passive: true })
        document.addEventListener('touchstart', this.onTouchStart, { passive: true })
        document.addEventListener('touchmove', this.onTouchMove, { passive: true })
        document.addEventListener('touchend', this.onTouchEnd, { passive: true })
    }

    componentWillUnmount() {
        document.removeEventListener('mousewheel', this.onMouseWheel);
        document.removeEventListener('wheel', this.onMouseWheel);
        document.removeEventListener('touchstart', this.onTouchStart);
        document.removeEventListener('touchmove', this.onTouchMove);
        document.removeEventListener('touchend', this.onTouchEnd);
    }

    componentDidUpdate(nextProps: Props) {

        if (nextProps.router.pathname !== this.props.router.pathname) {

            console.log(nextProps)

            const pageContent = this.pageContentRef.current;
            const pageNavigation = this.pageNavigationRef.current;
            const pageFooter = this.pageFooterRef.current;
            const sectionContainer = this.sectionContainerRef.current;
            const sections = this.getSections(nextProps)
            const backgroundColor = sections[this.step].props.background;
            const color = sections[this.step].props.color;

            gsap.set(pageContent, { visibility: "visible", backgroundColor: backgroundColor, color: color })
            gsap.set(pageNavigation, { color: color })

            this.showTitle(sections[this.step])

            this.setState({
                pageContent,
                pageNavigation,
                pageFooter,
                sectionContainer,
                sections
            })

        }
    }

    onPrevious = () => {
        this.change(this.step - 1);
    }

    onNext = () => {
        this.change(this.step + 1);
    }

    change = (index: number) => {

        if (this.isAnimating) return;
        const { pageContent, pageFooter, sectionContainer, sections } = this.state;
        const previous = sections[this.step];
        const next = sections[index];
        const isNextFooter = index === sections.length;
        let isCurrentFooter = false;
        if (!next && !isNextFooter) return;

        this.isAnimating = true;
        this.step = index;

        if (!isNextFooter) {

            isCurrentFooter = false;

            this.setState({
                isCurrentFooter
            });

            this.changeTitle(previous, next);

            gsap.to(sectionContainer, {
                duration: .8,
                y: `-${this.step * 100}%` == '-0%' ? 0 : `-${this.step * 100}%`,
                onComplete: () => {
                    this.onSectionChanged()
                }
            })

            this.changeBackground();

        }
        else {

            isCurrentFooter = true;

            this.setState({
                isCurrentFooter
            });

            gsap.to(pageContent, {
                duration: .8,
                y: - pageFooter.clientHeight,
                onComplete: () => this.onSectionChanged()
            })

        }

    }

    onMouseWheel = event => {

        if (this.lethargy.check(event) !== false) {
            const delta = event.wheelDelta || -event.deltaY
            if (delta < 0) {
                this.onNext()
            } else if (delta > 0) {
                this.onPrevious()
            }
        }

    }

    onTouchStart = (event) => {
        this.isTouchDown = true

        this.y.start = event.touches ? event.touches[0].clientY : event.clientY
        this.y.end = event.touches ? event.touches[0].clientY : event.clientY

    }

    onTouchMove = (event) => {
        if (!this.isTouchDown) return

        this.y.end = event.touches ? event.touches[0].clientY : event.clientY

    }

    onTouchEnd = () => {
        if (!this.isTouchDown) return

        this.isTouchDown = false

        if (this.y.start - 25 > this.y.end) {
            this.onNext()
        } else if (this.y.start + 25 < this.y.end) {
            this.onPrevious()
        }
    }

    getSections = (children: any) => {

       const sections = React.Children.map( children, (child, idx) => {

            const titlesTl = gsap.timeline({ paused: true });
            const letters = split({ element: this.titles[idx], expression: '', append: false });

            letters.forEach((letter: HTMLSpanElement) => {
                let yOffset = randomInteger(-100, 100)
                gsap.set(letter, {
                    y: `${yOffset}%`,
                    opacity: 0
                })
            })

            const title = titlesTl
                .set(this.titles[idx], {
                    delay: 1,
                    autoAlpha: 1,
                })
                .to(letters, {
                    duration: .5,
                    autoAlpha: 1,
                    y: "0%",
                    ease: "power3.out"
                })
                .reverse();

            return ( React.cloneElement(child, {title}) )

            
        })

        return sections;

    }

    onSectionChanged = () => {
        this.isAnimating = false
    }

    changeBackground = () => {

        const { pageContent, pageNavigation, sections } = this.state;
        const backgroundColor = sections[this.step].props.background;
        const color = sections[this.step].props.color;

        gsap.to(pageContent, {
            duration: 0.8,
            y: 0,
            backgroundColor: backgroundColor,
            color: color
        })

        gsap.to(pageNavigation, {
            duration: 0.8,
            color: color,
        })

    }

    showTitle = (current : React.ReactElement) => {
        current.props.title.play();
    }

    changeTitle = (previous : React.ReactElement, next : React.ReactElement) => {

        if (previous && previous.props.title) {
            previous.props.title.reverse();
        }

        if (next && next.props.title) {
            next.props.title.play();
        }

    }


    render() {

        const { template, children } = this.props;
        const { isCurrentFooter } = this.state;

        return (
            <>
                <div className="page-navigation" ref={this.pageNavigationRef}>
                    <PageNavigation isCurrentFooter={isCurrentFooter} />
                </div>

                <div className="page-content" ref={this.pageContentRef}>

                    {children && (
                        <div className="section-titles">
                            {React.Children.map(children, (child : {props: { title : string; }}, index) => (
                               <Title template={template} title={child?.props?.title || ''} ref={el => this.titles[index] = el}/>
                            ))}
                        </div>
                    )}

                    <div className={`section-container ${isCurrentFooter ? 'footer-visible' : ''}`} ref={this.sectionContainerRef}>
                        {children}
                    </div>

                </div>

                <div className="page-footer" ref={this.pageFooterRef}>
                    <PageFooter isCurrentFooter={isCurrentFooter} />
                </div>

            </>
        );
    }
};


export default withRouter(PageContent);