import { useEffect, useRef, useState } from 'react';
import { gsap } from 'gsap';
import Widget from './widget';
import Link from 'next/link';

const Footer = ({ isCurrentFooter, widgets, pages }) => {

    const footerInner = useRef<HTMLDivElement | null>(null);
    const [footerTween] = useState(gsap.timeline({ paused: true }));

    useEffect(() => {

        const footerInnerRef = footerInner.current;

        const footerAbove = footerInnerRef.children[0];
        const footerTop = footerInnerRef.children[1];
        const footerBottom = footerInnerRef.children[2];

        footerTween
            .to([footerAbove, footerTop, footerBottom], {
                duration: 1,
                ease: "power3.out",
                autoAlpha: 1,
                y: 0,
                stagger: {
                    amount: 0.15
                }
            })
            .reverse()

        return () => {
            footerTween.kill()
        }

    }, [])

    useEffect(() => {
        footerTween.reversed(!isCurrentFooter);
    }, [isCurrentFooter])

    return (

        <div className="page-footer__inner" ref={footerInner}>

            <div className="page-footer__top">
                {
                    widgets.map(({ id, title, content }) => <Widget title={title} content={content} key={id} />)
                }
            </div>

            <div className="page-footer__bottom">
                <ul>
                    {
                        pages.map(({ id, name, slug }) => {
                            return (
                                <li key={id}>
                                    <Link href={slug}>
                                        <a>
                                            <span>
                                                {name}
                                            </span>
                                        </a>
                                    </Link>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>

        </div>

    )

}

export default Footer;