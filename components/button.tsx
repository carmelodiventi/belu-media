
import { useEffect, FunctionComponent, useRef } from 'react';
import Link from 'next/link';
import { useAppContext } from 'contexts/AppContext';

type IProps = {
    target: {
        id: string;
        slug: string;
        name: string;
    }
}

const Button : FunctionComponent<IProps> = ({ target: { id, slug, name } }) => {

    const btnRef = useRef<HTMLAnchorElement | null>(null);
    const { dispatch } = useAppContext();

    useEffect(() => {

        btnRef.current.addEventListener('mouseenter', _onMouseEnter, true)
        btnRef.current.addEventListener('mouseleave', _onMouseLeave, true)

        return () => {
            btnRef.current.removeEventListener('mouseenter', _onMouseEnter)
            btnRef.current.removeEventListener('mouseleave', _onMouseLeave)
        }

    }, [])


    const _onMouseEnter = () => {
        dispatch({ type: "CHANGE_CURSOR", payload: { style: 'cursor--hover-link' } })
    }

    const _onMouseLeave = () => {
        dispatch({ type: "DEFAULT_CURSOR" })
    }

    return (
        <Link href={slug}>
            <a className="btn btn__link" data-id={id} ref={btnRef}>
                {name}
                <span></span>
                <span></span>
                <svg width="41" height="16" viewBox="0 0 41 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M40.7071 8.70711C41.0976 8.31658 41.0976 7.68342 40.7071 7.29289L34.3431 0.928932C33.9526 0.538408 33.3195 0.538408 32.9289 0.928932C32.5384 1.31946 32.5384 1.95262 32.9289 2.34315L38.5858 8L32.9289 13.6569C32.5384 14.0474 32.5384 14.6805 32.9289 15.0711C33.3195 15.4616 33.9526 15.4616 34.3431 15.0711L40.7071 8.70711ZM0 9L16.0714 9V7L0 7L0 9ZM16.0714 9L40 9V7L16.0714 7V9Z" fill="currentColor" />
                </svg>
            </a>
        </Link>
    )

}


export default Button