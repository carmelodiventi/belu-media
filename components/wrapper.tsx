interface IProps {
    children: React.ReactChildren
}

const Wrapper = ({ children } : IProps) => (
    
    <div className="page-canvas"> 
        {children} 
    </div>

)

export default Wrapper;