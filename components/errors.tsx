import React from 'react';
import Button from './button';

const Errors = ({ message }) => {
    return (
        <div className="error">
            <p className="error-message">
                {message}
            </p>
            <Button target={{
                id: "button-home",
                slug: '/',
                name: 'Back to Home'
            }} />
        </div>
    );
};

export default Errors;