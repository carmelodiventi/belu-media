// menu 
export const TOGGLE_MENU = 'TOGGLE_MENU';
// cursor 
export const DEFAULT_CURSOR = 'DEFAULT_CURSOR';
export const CHANGE_CURSOR = 'CHANGE_CURSOR';
// staff 
export const SHOW_MEMBER_INFO = 'SHOW_MEMBER_INFO';
export const HIDE_MEMBER_INFO = 'HIDE_MEMBER_INFO';
