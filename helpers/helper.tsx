
export function convertToID(text: string)
{
    return text.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
}

export function slugify(text: string)
{
    return text.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');
}

export function randomInteger(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}
