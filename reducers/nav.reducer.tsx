import { TOGGLE_MENU } from "../actions/actionTypes"

export const initialState = {
    navOpen: false,
}

export const reducer = (state = initialState, action: { type: any; payload: { navOpen: any } }) => {

    switch (action.type) {

        case TOGGLE_MENU:

            return {
                ...state,
                navOpen: action.payload.navOpen
            }

        default:
            return state
    }

}