import { DEFAULT_CURSOR, CHANGE_CURSOR } from "../actions/actionTypes"

export const initialState = {
    style: null,
}

export const reducer = (state = initialState, action: { type: any; payload: { style: any } }) => {

    switch (action.type) {

        case DEFAULT_CURSOR:

            return {
                style: null
            }

        case CHANGE_CURSOR:

            return {
                ...state,
                style: action.payload.style
            }


        default:
            return state
    }

}