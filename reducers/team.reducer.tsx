import { SHOW_MEMBER_INFO, HIDE_MEMBER_INFO } from '../actions/actionTypes';

export const initialState = {
    isHover: false,
    active: null
}

export const reducer =  (state = initialState, action: { type: any; payload: { index: any; }; }) => {

    switch (action.type) {

        case SHOW_MEMBER_INFO:
            return {
                ...state,
                isHover: true,
                active: action.payload.index
            }

        case HIDE_MEMBER_INFO:
            return {
                ...state,
                isHover: false,
                active: action.payload.index
            }
        default:
            return state;
    }

}