import { ReactNode, useEffect, useRef } from "react";

type Props = {
  children: ReactNode
}

const withFontResize = (WrappedComponent: React.ComponentType) => (props: Props) => {

  const size : number = 20;
  const fontSize = useRef<number | null>(null);

  useEffect(() => {
    resize()
    document.documentElement.style.fontSize = `${fontSize.current}px`;
    window.addEventListener("resize", resize);
    return () => {
      window.removeEventListener("resize", resize)
    }

  }, [])

  const resize = () => {

    const height = {
      current: window.innerHeight,
      limit: window.innerWidth < 768 ? window.innerWidth < window.innerHeight ? 1280 : 602 : 1600,
      scale: 0
    };

    const width = {
      current: window.innerWidth,
      limit: window.innerWidth < 768 ? window.innerWidth < window.innerHeight ? 750 : 1344 : 2560,
      scale: 0
    };

    height.scale = height.current / height.limit
    width.scale = width.current / width.limit

    fontSize.current = size * Math.min(height.scale, width.scale);

    document.documentElement.style.fontSize = `${fontSize}px`;

  }

  return ( 
    <WrappedComponent {...props}>{props.children}</WrappedComponent> 
  );

}

export default withFontResize;