import Footer from '../components/footer';
import useSWR from 'swr';
import request from 'graphql-request';
import { queryFooter } from '../graphql/queries';
import Errors from '@components/errors';
import Loading from '@components/loading';

interface IData {
    menu: {
        id: string;
        name: string;
        pages: [
            {
                title: string;
                name: string;
                slug: string;
            }
        ]
    };
    widgets: {
        content: [
            {
                id: string;
                title: string;
                content: string;
            }
        ]
    };
}

interface IQueryMenuVars {
    slug: string;
}

const PageFooter = ({ isCurrentFooter }) => {

    const { data, error } = useSWR(queryFooter, query => request(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, query, { slug: "bottom-menu" } ));

    if (error) return <Errors message={"Error to loading"} />
    if (!data) return <Loading />

    const { menu, widgets } = data;
    const { pages } = menu || null;
    const { content } = widgets || null;

    return (
        <Footer widgets={content} pages={pages} isCurrentFooter={isCurrentFooter} />
    )

}

export default PageFooter;