import useSWR from 'swr'
import { request } from 'graphql-request';
import { queryMenu } from '../graphql/queries';
import Nav from '@components/nav';
import Errors from '@components/errors';
import Loading from '@components/loading';

interface IData {
  menu: {
    id: string;
    name: string;
    pages: [
      {
        title: string;
        name: string;
        slug: string;
      }
    ]
  };
  widgets: {
    content: [
      {
        id: string;
        title: string;
        content: string;
      }
    ]
  };
}


const PageNav = () => {

  const { data, error } = useSWR(queryMenu, query => request<IData>(process.env.NEXT_PUBLIC_API_GRAPHQL_HOST, query, { slug: 'primary-menu'}));

  if (error) return <Errors message={"error to loading"} />
  if (!data) return <Loading />

  const { menu, widgets } = data;
  const { pages } = menu || null;
  const { content } = widgets || null;

  return <Nav pages={pages} widgets={content} />;

}


export default PageNav;